import { Component, OnInit } from '@angular/core';
import { TodoItem } from '../shared/models/todoItem.model';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent implements OnInit {
  
  todos: TodoItem[] = [
    new TodoItem("Todo A", "Description de Todo A"),
    new TodoItem("Todo B", "Description de Todo B"),
    new TodoItem("Todo C", "Description de Todo C un peu trop longue pour être affichée entièrement"),
    new TodoItem("Todo D", "Description de Todo D"),
  ]

  constructor() { }

  ngOnInit(): void {
  }

  handleNewTodo(newTodo: TodoItem) {
    this.todos.push(newTodo);
  }

  handleDeleteTodo(id: number) {
    this.todos.splice(id, 1);
    console.log(this.todos)
  }

}
