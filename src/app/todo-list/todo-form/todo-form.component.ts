import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { TodoItem } from 'src/app/shared/models/todoItem.model';

@Component({
  selector: 'app-todo-form',
  templateUrl: './todo-form.component.html',
  styleUrls: ['./todo-form.component.css']
})
export class TodoFormComponent implements OnInit {
  title!: string;
  description!: string;
  
  @Output('newTodo') newTodoEmitter = new EventEmitter<TodoItem>;


  constructor() { }

  ngOnInit(): void {
  }

  onSendNewTodo() {
    this.newTodoEmitter.emit(new TodoItem(this.title, this.description));
  }

}
