import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TodoItem } from 'src/app/shared/models/todoItem.model';

@Component({
  selector: '[app-todo-item]',
  templateUrl: './todo-item.component.html',
  styleUrls: ['./todo-item.component.css']
})
export class TodoItemComponent implements OnInit {
  
  @Input('todoId') id!: number;
  @Input('todo') todoItem!: TodoItem;

  @Output() deleteTodo = new EventEmitter<number>;

  constructor() { }

  ngOnInit(): void {
  }

  onDeleteTodo() {
    this.deleteTodo.emit(this.id);
  }

}
