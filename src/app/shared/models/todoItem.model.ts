export class TodoItem {
    public dateOfSubmit: Date;
    public isDone: boolean;

    constructor(public title: string, public description: string) {
        this.dateOfSubmit = new Date();
        this.isDone = false;
    }

    switchStatus() {
        this.isDone = !this.isDone;
    }
}